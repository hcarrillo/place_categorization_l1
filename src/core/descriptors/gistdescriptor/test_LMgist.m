clc
clear all
close all

% Load image
img = imread('test.jpg');
img_bw = rgb2gray(img);
%extract gist feature

% Parameters:
clear param
%param.imageSize = [256 256]; % This reduce and crop teh image to that size. It works also with non-square images
param.orientationsPerScale = [8 8 8 8];
param.numberBlocks = 4;
param.fc_prefilt = 4;
gGist_len = sum(param.orientationsPerScale)*param.numberBlocks*param.numberBlocks;

% Computing gist requires 1) prefilter image, 2) filter image and collect
% output energies
[gGist_temp, param] = LMgist(img_bw, '', param);
gGist = normc(gGist_temp');

% Visualization
figure
subplot(121)
imshow(img_bw)
title('Input image')
subplot(122)
showGist(gGist_temp, param)
title('Descriptor')

