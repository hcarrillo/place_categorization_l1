%% Classes
classes_label{1} = 'BO';
classes_label{2} = 'CR';
classes_label{3} = 'EO';
classes_label{4} = 'KT';
classes_label{5} = 'PR';

weather_level{1} = 'cloudy';
weather_level{2} = 'night';
weather_level{3} = 'sunny';