addpath(genpath('../core/descriptors/gistdescriptor'))
addpath(genpath('../core/l1/L1_code'))
addpath(genpath('../core/l1/l1magic'))

addpath(genpath('./dictionaries/INDECS'))
addpath(genpath('./dictionaries/INDECS/gist'))
%addpath(genpath('./dictionaries/INDECS/gist_reduced'))
addpath(genpath('./labels/INDECS'))

