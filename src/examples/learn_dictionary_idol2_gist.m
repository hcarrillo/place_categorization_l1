clc
clear all
close all

run classes_idol2.m

%% Configure Images
dataset_path = '~/datasets/IDOL2/';

for indx_platform = 1:1:2
    for indx_weather = 1:1:3
        for indx_class_type = 1:1:5
            
platform_type = indx_platform;
%[1 2]            
weather_type = indx_weather;
%[1 2 3]
class_type = indx_class_type;
%[1 2 3 4 5]

working_path = [dataset_path plataform_label{platform_type} '/' weather_level{weather_type} '/' classes_label{class_type}  ];

 step_image = 1;
 resize_to = 1/1;
 resize_percent = resize_to;
 originalHeight = 240;
 originalWidth = 309;
 resizedWidth = ceil(originalWidth*resize_percent);
 resizedHeight = ceil(originalHeight*resize_percent);
 fprintf('Resized Width %d\n',resizedWidth);
 fprintf('Resized Height %d\n',resizedHeight);
 
%%
folder_content = dir([working_path]);
numberOfImages = length(folder_content(not([folder_content.isdir])));

%% Configure GIST
clear param
param.imageSize = [resizedWidth resizedHeight]; % it works also with non-square images
param.orientationsPerScale = [8 8 8 8];
param.numberBlocks = 4;
param.fc_prefilt = 4;

%% Configure l1
decriptor_len = 512;
D = zeros(decriptor_len,numberOfImages,'single');

%%
idx = 1;
totalNumberOfImages =  numberOfImages;
for i=1:1:length(folder_content)
    if ~(folder_content(i).isdir)
        imToRead = [working_path,'/',folder_content(i).name];
        %disp(imToRead)
        im_current = rgb2gray(imresize(imread(imToRead),resize_percent));
        [im_vector, param] = LMgist(im_current, '', param);
        im_vector = im_vector'./norm(im_vector);
        D(:,idx)=im_vector;
        idx = idx+1;
        fprintf('Processed %d of %d\n',idx-1,totalNumberOfImages);
    end     
end
%%
save([plataform_label{platform_type},'_',classes_label{class_type},'_',weather_level{weather_type},'_Gist.mat'],'D')
        end
    end
end
