addpath(genpath('../core/descriptors/gistdescriptor'))
addpath(genpath('../core/l1/L1_code'))
addpath(genpath('../core/l1/l1magic'))
addpath(genpath('../core/others/figure_related'))

addpath(genpath('./dictionaries/IDOL2'))
addpath(genpath('./dictionaries/IDOL2/gist'))
addpath(genpath('./labels/IDOL2'))
