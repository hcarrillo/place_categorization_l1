
% dic_label{1} = 'BO_sunny_Gist.mat';
% dic_label{2} = 'CR_sunny_Gist.mat';
% dic_label{3} = 'EO_sunny_Gist.mat';
% dic_label{4} = 'KT_sunny_Gist.mat';
% dic_label{5} = 'PR_sunny_Gist.mat';

dic_label{1} = 'BO_night_Gist.mat';
dic_label{2} = 'CR_night_Gist.mat';
dic_label{3} = 'EO_night_Gist.mat';
dic_label{4} = 'KT_night_Gist.mat';
dic_label{5} = 'PR_night_Gist.mat';

% dic_label{1} = 'BO_cloudy_Gist.mat';
% dic_label{2} = 'CR_cloudy_Gist.mat';
% dic_label{3} = 'EO_cloudy_Gist.mat';
% dic_label{4} = 'KT_cloudy_Gist.mat';
% dic_label{5} = 'PR_cloudy_Gist.mat';

%%
% dic_label{1} = 'reduced_20_BO_cloudy_Gist.mat';
% dic_label{2} = 'reduced_20_CR_cloudy_Gist.mat';
% dic_label{3} = 'reduced_20_EO_cloudy_Gist.mat';
% dic_label{4} = 'reduced_20_KT_cloudy_Gist.mat';
% dic_label{5} = 'reduced_20_PR_cloudy_Gist.mat';

% dic_label{1} = 'reduced_20_BO_night_Gist.mat';
% dic_label{2} = 'reduced_20_CR_night_Gist.mat';
% dic_label{3} = 'reduced_20_EO_night_Gist.mat';
% dic_label{4} = 'reduced_20_KT_night_Gist.mat';
% dic_label{5} = 'reduced_20_PR_night_Gist.mat';

% dic_label{1} = 'reduced_20_BO_sunny_Gist.mat';
% dic_label{2} = 'reduced_20_CR_sunny_Gist.mat';
% dic_label{3} = 'reduced_20_EO_sunny_Gist.mat';
% dic_label{4} = 'reduced_20_KT_sunny_Gist.mat';
% dic_label{5} = 'reduced_20_PR_sunny_Gist.mat';

%%
% dic_label{1} = 'reduced_15_BO_cloudy_Gist.mat';
% dic_label{2} = 'reduced_15_CR_cloudy_Gist.mat';
% dic_label{3} = 'reduced_15_EO_cloudy_Gist.mat';
% dic_label{4} = 'reduced_15_KT_cloudy_Gist.mat';
% dic_label{5} = 'reduced_15_PR_cloudy_Gist.mat';

% dic_label{1} = 'reduced_15_BO_night_Gist.mat';
% dic_label{2} = 'reduced_15_CR_night_Gist.mat';
% dic_label{3} = 'reduced_15_EO_night_Gist.mat';
% dic_label{4} = 'reduced_15_KT_night_Gist.mat';
% dic_label{5} = 'reduced_15_PR_night_Gist.mat';

% dic_label{1} = 'reduced_15_BO_sunny_Gist.mat';
% dic_label{2} = 'reduced_15_CR_sunny_Gist.mat';
% dic_label{3} = 'reduced_15_EO_sunny_Gist.mat';
% dic_label{4} = 'reduced_15_KT_sunny_Gist.mat';
% dic_label{5} = 'reduced_15_PR_sunny_Gist.mat';
%%
% dic_label{1} = 'reduced_12.5_BO_cloudy_Gist.mat';
% dic_label{2} = 'reduced_12.5_CR_cloudy_Gist.mat';
% dic_label{3} = 'reduced_12.5_EO_cloudy_Gist.mat';
% dic_label{4} = 'reduced_12.5_KT_cloudy_Gist.mat';
% dic_label{5} = 'reduced_12.5_PR_cloudy_Gist.mat';

% dic_label{1} = 'reduced_12.5_BO_night_Gist.mat';
% dic_label{2} = 'reduced_12.5_CR_night_Gist.mat';
% dic_label{3} = 'reduced_12.5_EO_night_Gist.mat';
% dic_label{4} = 'reduced_12.5_KT_night_Gist.mat';
% dic_label{5} = 'reduced_12.5_PR_night_Gist.mat';

% dic_label{1} = 'reduced_12.5_BO_sunny_Gist.mat';
% dic_label{2} = 'reduced_12.5_CR_sunny_Gist.mat';
% dic_label{3} = 'reduced_12.5_EO_sunny_Gist.mat';
% dic_label{4} = 'reduced_12.5_KT_sunny_Gist.mat';
% dic_label{5} = 'reduced_12.5_PR_sunny_Gist.mat';

%%
% dic_label{1} = 'reduced_10_BO_cloudy_Gist.mat';
% dic_label{2} = 'reduced_10_CR_cloudy_Gist.mat';
% dic_label{3} = 'reduced_10_EO_cloudy_Gist.mat';
% dic_label{4} = 'reduced_10_KT_cloudy_Gist.mat';
% dic_label{5} = 'reduced_10_PR_cloudy_Gist.mat';
% 
% dic_label{1} = 'reduced_10_BO_night_Gist.mat';
% dic_label{2} = 'reduced_10_CR_night_Gist.mat';
% dic_label{3} = 'reduced_10_EO_night_Gist.mat';
% dic_label{4} = 'reduced_10_KT_night_Gist.mat';
% dic_label{5} = 'reduced_10_PR_night_Gist.mat';

% dic_label{1} = 'reduced_10_BO_sunny_Gist.mat';
% dic_label{2} = 'reduced_10_CR_sunny_Gist.mat';
% dic_label{3} = 'reduced_10_EO_sunny_Gist.mat';
% dic_label{4} = 'reduced_10_KT_sunny_Gist.mat';
% dic_label{5} = 'reduced_10_PR_sunny_Gist.mat';
%%
% dic_label{1} = 'reduced_7.5_BO_cloudy_Gist.mat';
% dic_label{2} = 'reduced_7.5_CR_cloudy_Gist.mat';
% dic_label{3} = 'reduced_7.5_EO_cloudy_Gist.mat';
% dic_label{4} = 'reduced_7.5_KT_cloudy_Gist.mat';
% dic_label{5} = 'reduced_7.5_PR_cloudy_Gist.mat';

% dic_label{1} = 'reduced_7.5_BO_night_Gist.mat';
% dic_label{2} = 'reduced_7.5_CR_night_Gist.mat';
% dic_label{3} = 'reduced_7.5_EO_night_Gist.mat';
% dic_label{4} = 'reduced_7.5_KT_night_Gist.mat';
% dic_label{5} = 'reduced_7.5_PR_night_Gist.mat';

% dic_label{1} = 'reduced_7.5_BO_sunny_Gist.mat';
% dic_label{2} = 'reduced_7.5_CR_sunny_Gist.mat';
% dic_label{3} = 'reduced_7.5_EO_sunny_Gist.mat';
% dic_label{4} = 'reduced_7.5_KT_sunny_Gist.mat';
% dic_label{5} = 'reduced_7.5_PR_sunny_Gist.mat';

%%
% dic_label{1} = 'reduced_5_BO_cloudy_Gist.mat';
% dic_label{2} = 'reduced_5_CR_cloudy_Gist.mat';
% dic_label{3} = 'reduced_5_EO_cloudy_Gist.mat';
% dic_label{4} = 'reduced_5_KT_cloudy_Gist.mat';
% dic_label{5} = 'reduced_5_PR_cloudy_Gist.mat';

% dic_label{1} = 'reduced_5_BO_night_Gist.mat';
% dic_label{2} = 'reduced_5_CR_night_Gist.mat';
% dic_label{3} = 'reduced_5_EO_night_Gist.mat';
% dic_label{4} = 'reduced_5_KT_night_Gist.mat';
% dic_label{5} = 'reduced_5_PR_night_Gist.mat';

% dic_label{1} = 'reduced_5_BO_sunny_Gist.mat';
% dic_label{2} = 'reduced_5_CR_sunny_Gist.mat';
% dic_label{3} = 'reduced_5_EO_sunny_Gist.mat';
% dic_label{4} = 'reduced_5_KT_sunny_Gist.mat';
% dic_label{5} = 'reduced_5_PR_sunny_Gist.mat';

