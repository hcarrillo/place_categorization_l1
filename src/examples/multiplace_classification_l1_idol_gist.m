clc
clear all
close all

run classes_idol2.m

%% Configure Images
%dataset_path = '~/datasets/IDOL2/';
dataset_path = '/media/henry/40326ffe-ed7e-4c8b-86f1-70a4ba8e7e0b/datasets/IDOL2/';

for weather_type_indx=1:1:3
platform_type = 2;
%[1 2]
weather_dic = 3;
weather_type = weather_type_indx;
%[1 2 3]
class_type = [1 2 3 4 5];
%[1 2 3 4 5]

 step_image = 1;
 resize_to = 1/1;
 resize_percent = resize_to;
 originalHeight = 240;
 originalWidth = 309;
 resizedWidth = ceil(originalWidth*resize_percent);
 resizedHeight = ceil(originalHeight*resize_percent);
 fprintf('Resized Width %d\n',resizedWidth);
 fprintf('Resized Height %d\n',resizedHeight);

 
 %% Configure GIST
clear param
param.imageSize = [resizedWidth resizedHeight]; % it works also with non-square images
param.orientationsPerScale = [8 8 8 8];
param.numberBlocks = 4;
param.fc_prefilt = 4;

%% Configure l1
lambda = 0.01; 
% lambda= 0.01; Leads to less-sparse solution
% lambda = 1;
tol = 1e-10;
nu = 10;
stopCrit = 3;
maxIter = 5000;

decriptor_len = 512;
I_size = decriptor_len;

elapsed_time = [];
x = [];

I =eye(I_size,'single');

%% Load dictionaries
D = [];
dictionary_len(1) = 1; 

DA = load('minnie_BO_sunny_Gist.mat');
dictionary_len(2) =  size(DA.D,2) + size(D,2); 
D=[D DA.D];

DA = load('minnie_CR_sunny_Gist.mat');
dictionary_len(3) =  size(DA.D,2) + size(D,2); 
D=[D DA.D];

DA = load('minnie_EO_sunny_Gist.mat');
dictionary_len(4) =  size(DA.D,2) + size(D,2); 
D=[D DA.D];

DA = load('minnie_KT_sunny_Gist.mat');
dictionary_len(5) =  size(DA.D,2) + size(D,2); 
D=[D DA.D];

DA = load('minnie_PA_sunny_Gist.mat');
dictionary_len(6) =  size(DA.D,2) + size(D,2); 
D=[D DA.D];

dictionary_len(7) =  size(D,2) + size(I,2); 
%%
conf_mat_pdf = zeros(size(class_type,2)+1,size(class_type,2)+1);
conf_mat_max = zeros(size(class_type,2)+1,size(class_type,2)+1);
idx = 1;
totalNumberOfImages = zeros(1,size(class_type,2));
rslt_class = zeros(1,size(class_type,2));
rslt_class_max = zeros(1,size(class_type,2));
vec_prob = [];
vec_prob_change=[];
for indx_class = 1:1:size(class_type,2)
    working_path = [dataset_path plataform_label{platform_type} '/' weather_level{weather_type} '/' classes_label{indx_class}  ];
    folder_content = dir(working_path);
    numberOfImages = length(folder_content(not([folder_content.isdir])));
    x = [x zeros(size(D,1)+size(D,2),numberOfImages,'single')];        
    totalNumberOfImages(indx_class) =  totalNumberOfImages(indx_class) + numberOfImages;
    for i=1:1:numberOfImages  
        if ~(folder_content(i).isdir)
            imToRead = [working_path,'/',folder_content(i).name];
            %disp(imToRead)
            im_current = rgb2gray(imresize(imread(imToRead),resize_percent));
            [im_vector, param] = LMgist(im_current, '', param);
            im_vector = im_vector'./norm(im_vector');
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %startTime = tic;
            x_n = SolveHomotopy([D I],im_vector,'lambda',lambda,'tolerance',tol);
            %elapsed_time(idx) = toc(startTime);
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%% PDF style
            x(1:length(x_n),idx) = (x_n./norm(x_n)).^2;
            fx = (x_n./norm(x_n)).^2;
            Fx = [0; cumsum(fx); 1];

            for indx_prob_class = 1:1:size(dictionary_len,2)-1
                rslt_prob_temp(indx_prob_class) = Fx(dictionary_len(indx_prob_class+1)) - Fx(dictionary_len(indx_prob_class));
            end
            [val_max_prob,indx_max] = max(rslt_prob_temp);
            vec_prob_wrong(idx) = 0;
            vec_prob_good(idx) = 0;
            %vec_prob{indx_class}.prob(indx_prob) = Fx(size(D,2));
            if(indx_class == indx_max(1))
                rslt_class(indx_class) = rslt_class(indx_class) + 1;
                vec_prob_good(idx) = val_max_prob;
            else
                vec_prob_wrong(idx) = val_max_prob;
            end
            vec_prob(idx) = val_max_prob;
             conf_mat_pdf(indx_class,indx_max(1)) =  conf_mat_pdf(indx_class,indx_max(1)) + 1;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%% Max style
            [~,indx_max_fx] = max(fx);

            for indx_prob_class_fx = 1:1:size(dictionary_len,2)-1
                rslt_prob_temp_max(indx_prob_class_fx) = 0;
                if( indx_max_fx <= dictionary_len(indx_prob_class_fx+1) &&  indx_max_fx >= dictionary_len(indx_prob_class_fx) )
                     rslt_prob_temp_max(indx_prob_class_fx) = 1;
                end
            end
            [~,indx_max_fx_v] = max(rslt_prob_temp_max);
            if (indx_class == indx_max_fx_v(1))
                rslt_class_max(indx_class) = rslt_class_max(indx_class) + 1;
            end
            conf_mat_max(indx_class,indx_max_fx_v(1)) =  conf_mat_max(indx_class,indx_max_fx_v(1)) + 1;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            idx = idx+1;
            fprintf('Processed %d of %d\n',idx-1,sum(totalNumberOfImages));  
        end 
    end
         vec_prob_change(indx_class)=idx-1;
 end


%%
accuraccy = (rslt_class./totalNumberOfImages)*100
mean_accuraccy = mean(accuraccy)

accuraccy_max = (rslt_class_max./totalNumberOfImages)*100
mean_accuraccy_max = mean(accuraccy_max)

figure(1)
spy(x)

figure(2)
spy(x>0.1)

%%
save([plataform_label{platform_type},'_',weather_level{weather_dic},'_',weather_level{weather_type},'_rslt.mat'])
end