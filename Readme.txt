# Welcome

This repository hosts the code used to produce the experiments in the paper "Place Categorization Using Sparse and Redundant Representations"

```
#!latex

@inproceedings{Carrillo2014,
	author = "Carrillo, H. and Latif, Y. and Neira, J. and Castellanos, J.A.",
	booktitle = "{Proceedings of the IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS)}",
	issn = "xxx",
	month = "Sep",
	pages = "TBP",
	title = "{Place Categorization Using Sparse and Redundant Representations}",
	year = "2014"
}
```

## Code

* **addPathAll_IDOL2.m** :: This will add the relevant paths to search for the code needed for using the IDOL dataset.

* **addPathAll_INDECS.m** :: This will add the relevant paths to search for the code needed for using the INDECS dataset.

* **classes_idol2.m** :: This will set the variables to handle the classes of the IDOL dataset.

* **classes_indecs.m** :: This will set the variables to handle the classes of the INDECS dataset.

* **learn_dictionary_idol2_gist.m** :: This will create dictionaries from the IDOL dataset. For the experiments, we test the learned dictionary in one class against the rest, therefore this script allows to learn dictionaries in a per class basis for the IDOL dataset.
The dictionary will be store as a mat file with a name that describe the classes used.

* **learn_dictionary_indecs_gist.m** :: This will create dictionaries from the INDECS dataset. For the experiments, we test the learned dictionary in one class against the rest, therefore this script allows to learn dictionaries in a per class basis for the INDECS dataset.
The dictionary will be store as a mat file with a name that describe the classes used.

* **dictionaries_indecs.m** :: This will set the different variables needed to decode the names of the dictionaries generated for the INDECS dataset.

* **multiplace_classification_l1_idol_gist.m** :: This will perform the l1-norm based place categorization task on the IDOL dataset, using a designated dictionary. The results of the categorization will be store as a mat file with a name that describe the classes used.

* **multiplace_classification_l1_indecs_gist.m** :: This will perform the l1-norm based place categorization task on the INDECS dataset, using a designated dictionary. The results of the categorization will be store as a mat file with a name that describe the classes used.

* **get_metrics.m** :: This will print on the screen useful metrics of the results.

## Datasets (re)organization

* **IDOL dataset** :: In order to facilitate the processing of the data, the original dataset need to be reorganized, in folders, as follow:

* 1° level : (robots) dumbo  minnie
* 2° level : (weather) cloudy night sunny
* 3° level : (category) BO CR EO KT PA

in the lower level the images will be placed.

* **INDECS dataset** :: In order to facilitate the processing of the data, the original dataset need to be reorganized, in folders, as follow:

* 1° level : (category) BO CR EO KT PA
* 2° level : (weather) cloudy night sunny

in the lower level the images will be placed.

## Contact

Please do not hesitate to contact if you need help or has comments:

* hcarri at unizar dot es

 
